package main

import "testing"

func Test_romanToDecimal(t *testing.T) {
	type args struct {
		roman string
	}
	tests := []struct {
		name    string
		args    args
		want    int
		wantErr bool
	}{
		// TODO: Add test cases.
		{
			name:    "Basic addition",
			args:    args{"III"},
			want:    3,
			wantErr: false,
		},
		{
			name:    "Subtraction",
			args:    args{"IV"},
			want:    4,
			wantErr: false,
		},
		{
			name:    "Combination",
			args:    args{"IX"},
			want:    9,
			wantErr: false,
		},
		{
			name:    "Large number",
			args:    args{"MCMXCIV"},
			want:    1994,
			wantErr: false,
		},
		{
			name:    "Single symbol",
			args:    args{"V"},
			want:    5,
			wantErr: false,
		},
		{
			name:    "Invalid symbol",
			args:    args{"Z"},
			want:    0,
			wantErr: true,
		},
		{
			name:    "Repeated symbol limit",
			args:    args{"IIII"},
			want:    0,
			wantErr: true,
		},
		{
			name:    "Empty string",
			args:    args{""},
			want:    0,
			wantErr: true,
		},
		{
			name:    "Barred numeral",
			args:    args{"_X"},
			want:    10000,
			wantErr: false,
		},
		{
			name:    "Barred numeral with normal numeral",
			args:    args{"_IX"},
			want:    1010,
			wantErr: false,
		},
		{
			name:    "Barred numeral combination",
			args:    args{"_I_X"},
			want:    9000,
			wantErr: false,
		},
		{
			name:    "Invalid barred numeral (repeated underscore)",
			args:    args{"__X"},
			want:    0,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := romanToDecimal(tt.args.roman)
			if (err != nil) != tt.wantErr {
				t.Errorf("romanToDecimal() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("romanToDecimal() = %v, want %v", got, tt.want)
			}
		})
	}
}
