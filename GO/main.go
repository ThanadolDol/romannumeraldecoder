package main

import (
	"fmt"
)

func romanToDecimal(roman string) (int, error) {
	if roman == "" {
		return 0, fmt.Errorf("roman numeral expression is empty")
	}

	romanValues := map[string]int{
		"I": 1, "V": 5, "X": 10, "L": 50, "C": 100, "D": 500, "M": 1000,
	}

	total := 0
	romanLength := len(roman) - 1
	prevValue := 0
	prevSymbol := string(roman[romanLength])
	hasBar := false // Flag to track if the previous numeral has a bar
	repeatedCount := 0

	for i := romanLength; i >= 0; i-- {
		currentSymbol := string(roman[i])

		if currentSymbol == prevSymbol {
			repeatedCount++
		} else {
			repeatedCount = 1
		}

		if repeatedCount > 3 {
			return 0, fmt.Errorf("invalid Roman numeral: %s", currentSymbol)
		}

		currentValue, exists := romanValues[currentSymbol]

		if !exists {
			return 0, fmt.Errorf("invalid Roman numeral: %s", currentSymbol)
		}

		if i > 0 && string(roman[i-1]) == "_" {
			hasBar = true
			i--
		}

		if hasBar {
			currentValue = currentValue * 1000
			hasBar = false
		}

		if currentValue < prevValue {
			total -= currentValue
		} else {
			total += currentValue
		}

		prevSymbol = currentSymbol
		prevValue = currentValue
	}

	return total, nil
}

func main() {
	romanNumerals := []string{"", "III", "LVIIII", "MCMXCIV", "IX", "_IV", "_XIV", "_XCIX", "MMMCMXCIX", "M_V"}

	for _, numeral := range romanNumerals {
		decimal, err := romanToDecimal(numeral)
		if err != nil {
			fmt.Printf("Error converting %s: %s\n", numeral, err)
		} else {
			fmt.Printf("%s = %d\n", numeral, decimal)
		}
	}
}
