export function romanToDecimal(roman: string): [number | null, string | null] {
  if (roman === "" || roman === null || roman === undefined){
    return [null, `Roman Numeral expression is empty`];
  }

  const romanValues: Record<string, number> = {
    I: 1,
    V: 5,
    X: 10,
    L: 50,
    C: 100,
    D: 500,
    M: 1000,
  };

  let total = 0;
  let prevValue = 0;
  const romanLength = roman.length - 1;
  let prevSymbol = roman[romanLength];
  let hasBar = false;
  let repeatedCount = 0;

  for (let i = romanLength; i >= 0; i--) {
    const currentSymbol = roman[i];

    if (currentSymbol === prevSymbol) {
      repeatedCount++;
    } else {
      repeatedCount = 1;
    }

    if (repeatedCount > 3) {
      return [null, `Invalid Roman numeral: ${currentSymbol}`];
    }

    let currentValue = romanValues[currentSymbol];

    if (currentValue === undefined) {
      return [null, `Invalid Roman numeral: ${currentSymbol}`];
    }

    if (i > 0 && roman[i - 1] === "_") {
      hasBar = true;
      i--; // Skip the underscore
    }

    if (hasBar) {
			currentValue = currentValue * 1000
			hasBar = false
		}

		if (currentValue < prevValue) {
			total -= currentValue
		} else {
			total += currentValue
		}

    prevSymbol = currentSymbol;
    prevValue = currentValue; 
  }

  return [total, null]; // Return the result and null for no error
}

const romanNumerals = ["IC", "III", "LVIIII", "MCMXCIV", "IX", "_IV", "_XIV", "__X", "MMMCMXCIX", "M_V"];

for (const numeral of romanNumerals) {
  const [decimal, error] = romanToDecimal(numeral);
  if (error) {
    console.error(`Error converting ${numeral}: ${error}`);
  } else {
    console.log(`${numeral} = ${decimal}`);
  }
}
