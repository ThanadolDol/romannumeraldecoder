import { romanToDecimal } from ".";

describe('romanToDecimal', () => {
    it('should handle basic addition (III)', () => {
      expect(romanToDecimal("III")).toEqual([3, null]);
    });
  
    it('should handle subtraction (IV)', () => {
      expect(romanToDecimal("IV")).toEqual([4, null]);
    });
  
    it('should handle combination (IX)', () => {
      expect(romanToDecimal("IX")).toEqual([9, null]);
    });
  
    it('should handle large numbers (MCMXCIV)', () => {
      expect(romanToDecimal("MCMXCIV")).toEqual([1994, null]);
    });
  
    it('should handle single symbols (V)', () => {
      expect(romanToDecimal("V")).toEqual([5, null]);
    });
  
    it('should handle invalid symbols (Z)', () => {
      expect(romanToDecimal("Z")).toEqual([null, "Invalid Roman numeral: Z"]);
    });
  
    it('should handle repeated symbol limits (IIII)', () => {
      expect(romanToDecimal("IIII")).toEqual([null, "Invalid Roman numeral: I"]);
    });
  
    it('should handle empty strings', () => {
      expect(romanToDecimal("")).toEqual([null, "Roman Numeral expression is empty"]);
    });
  
    it('should handle barred numerals (_X)', () => {
      expect(romanToDecimal("_X")).toEqual([10000, null]);
    });
  
    it('should handle barred numerals with normal numerals (_IX)', () => {
      expect(romanToDecimal("_IX")).toEqual([1010, null]);
    });
  
    it('should handle barred numeral combinations (_I_X)', () => {
      expect(romanToDecimal("_I_X")).toEqual([9000, null]);
    });
  
    it('should handle invalid barred numerals (__X)', () => {
      expect(romanToDecimal("__X")).toEqual([null, "Invalid Roman numeral: _"]);
    });
});