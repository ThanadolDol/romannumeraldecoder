# Roman Numeral Decoder

This project provides GO and TypeScript function to decode Roman numerals into their corresponding decimal (Arabic) values. The decoder handles standard Roman numeral rules as well as some extended features like barred numerals.